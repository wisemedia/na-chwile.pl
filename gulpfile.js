var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('styles-list', function() {
	gulp.src('wp-content/themes/creditosparaemprendedores/**/*.scss')
		.pipe(sass({
			outputStyle: 'expanded'
		}).on('error', sass.logError))
		.pipe(gulp.dest('wp-content/themes/creditosparaemprendedores/'));
});

//Watch task
gulp.task('default',function() {
    gulp.watch('wp-content/themes/creditosparaemprendedores/**/*.scss',['styles-list']);
});