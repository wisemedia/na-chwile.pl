<?php
/*
Plugin Name: Custom Post Types, Taxonomies and Shortcodes
Plugin URI: -
Description: Register all necessary Custom Post Types, Taxonomies and Shortcodes
Version: 0.5
Author: Theme Owner
Author URI:
License: GPL
Copyright: Theme Owner
*/

if( !class_exists('Cptts') ):

class Cptts {

	private $json;


	function __construct()
	{
		$this->json = $this->get_settings_json();

		add_action( 'admin_init', array( $this, 'load_textdomain' ) );

		add_action( 'init', array( $this, 'create_post_types' ) );
	}


	function load_textdomain() {
		load_plugin_textdomain( 'cptts', false, 'cptts/languages' );
	}


	public function create_post_types()
	{
		if( !empty($this->json) ) {
			if( $this->json->create_post_type ) {

				foreach( $this->json->create_post_type as $name => $post_type ) {

					$labels = array(
						'name' => _x($post_type->label_single, 'post type general name', 'cptts-admin'),
						'singular_name' => _x($post_type->label_single, 'post type singular name', 'cptts-admin'),
						'add_new' => _x('Add New', $post_type->label_single, 'cptts-admin'),
						'add_new_item' => __('Add New ' . $post_type->label_single, 'cptts-admin'),
						'edit_item' => __('Edit ' . $post_type->label_single, 'cptts-admin'),
						'new_item' => __('New ' . $post_type->label_single, 'cptts-admin'),
						'all_items' => __('All ' . $post_type->label_multi, 'cptts-admin'),
						'view_item' => __('View ' . $post_type->label_single, 'cptts-admin'),
						'search_items' => __('Search ' . $post_type->label_multi, 'cptts-admin'),
						'not_found' => __('No ' . $post_type->label_multi . ' found', 'cptts-admin'),
						'not_found_in_trash' => __('No ' . $post_type->label_multi . ' found in Trash', 'cptts-admin'),
						'parent_item_colon' => __('Parent ' . $post_type->label_multi . ':', 'cptts-admin'),
						'menu_name' => __($post_type->menu_name, 'cptts-admin')
					);

					$arg_slug = strtolower($post_type->label_single);

					if( isset($post_type->description) ) {
						$arg_description = $post_type->description;
					} else {
						$arg_description = '';
					}

					if( isset($post_type->public) && is_bool($post_type->public)  ) {
						$arg_public = $post_type->public;
					} else {
						$arg_public = true;
					}

					if( isset($post_type->exclude_from_search) && is_bool($post_type->exclude_from_search) ) {
						$arg_exclude_from_search = $post_type->exclude_from_search;
					} else {
						$arg_exclude_from_search = !$arg_public;
					}

					if( isset($post_type->publicly_queryable) && is_bool($post_type->publicly_queryable) ) {
						$arg_publicly_queryable = $post_type->publicly_queryable;
					} else {
						$arg_publicly_queryable = true;
					}

					if( isset($post_type->show_ui) && is_bool($post_type->show_ui) ) {
						$arg_show_ui = $post_type->show_ui;
					} else {
						$arg_show_ui = true;
					}

					if( isset($post_type->show_in_menu) && is_bool($post_type->show_in_menu) ) {
						$arg_show_in_menu = $post_type;
					} else {
						$arg_show_in_menu = $post_type->show_in_menu;
					}

					if( isset($post_type->show_in_admin_bar) && is_bool($post_type->show_in_admin_bar) ) {
						$arg_show_in_admin_bar = $post_type->show_in_admin_bar;
					} else {
						$arg_show_in_admin_bar = $arg_show_in_menu;
					}

					if( isset($post_type->query_var) && is_bool($post_type->query_var) ) {
						$arg_query_var = $post_type->query_var;
					} else {
						$arg_query_var = true;
					}

					if( isset($post_type->capability_type) ) {
						$arg_capability_type = $post_type->capability_type;
					} else {
						$arg_capability_type = 'post';
					}

					if( isset($post_type->hierarchical) ) {
						$arg_hierarchical = $post_type->hierarchical;
					}else{
						$arg_hierarchical = false;
					}

					if( isset($post_type->menu_position) ) {
						$arg_menu_position = $post_type->menu_position;
					} else {
						$arg_menu_position = null;
					}

					if( isset($post_type->menu_icon) ) {
						$arg_menu_icon = $post_type->menu_icon;
					} else {
						$arg_menu_icon = null;
					}

					if( isset($post_type->has_archive) ) {
						$arg_has_archive = $post_type->has_archive;
					}else{
						$arg_has_archive = false;
					}

					if( isset($post_type->supports) ) {
						$arg_supports = $post_type->supports;
					}else{
						$arg_supports = array('title', 'editor', 'thumbnail','page-attributes');
					}

					if( isset($post_type->has_archive) && is_bool($post_type->has_archive) ) {
						$arg_has_archive = $post_type->has_archive;
					} else {
						$arg_has_archive = false;
					}

					if( isset($post_type->can_export) && is_bool($post_type->can_export) ) {
						$arg_can_export = $post_type->can_export;
					} else {
						$arg_can_export = true;
					}

					$taxonomies = array();
					if( isset($post_type->taxonomies) ) {
						if( is_array($post_type->taxonomies) )
							$taxonomies = $post_type->taxonomies;

						if( is_string($post_type->taxonomies) )
							$taxonomies = array($post_type->taxonomies);
					}

					$args = array(
						'labels' => $labels,
						'description' => $arg_description,
						'public' => $arg_public,
						'exclude_from_search' => $arg_exclude_from_search,
						'publicly_queryable' => $arg_publicly_queryable,
						'show_ui' => $arg_show_ui,
						'show_in_menu' => $arg_show_in_menu,
						'show_in_admin_bar' => $arg_show_in_admin_bar,
						'query_var' => $arg_query_var,
						'rewrite' => array('slug' => _x($post_type->slug, 'URL slug', 'cptts-admin')),
						'capability_type' => $arg_capability_type,
						'has_archive' => $arg_has_archive,
						'hierarchical' => $arg_hierarchical,
						'menu_position' => $arg_menu_position,
						'menu_icon' => $arg_menu_icon,
						'supports' => $arg_supports,
						'has_archive' => $arg_has_archive,
						'can_export' => $arg_can_export,
						'taxonomies' => $taxonomies
					);

					register_post_type($name, $args);

					if( $post_type->tax != false ) {
						$this->create_taxonomies($name, $post_type);
					}

				}

			}
		} else {
			add_action( 'admin_notices', array( $this, 'json_error') );
		}
	}


	public function create_taxonomies($post_name, $post)
	{
		foreach( $post->tax[0] as $tax_name => $taxonomy ) {

			$tax_labels = array(
				'name' => _x( $taxonomy->tax_multi, 'taxonomy general name' ),
				'singular_name' => _x( $taxonomy->tax_single, 'taxonomy singular name' ),
				'search_items' =>  __( 'Search '.$taxonomy->tax_multi ),
				'all_items' => __( 'All '.$taxonomy->tax_multi ),
				'parent_item' => __( 'Parent '.$taxonomy->tax_single ),
				'parent_item_colon' => __( 'Parent '.$taxonomy->tax_single.':' ),
				'edit_item' => __( 'Edit '.$taxonomy->tax_single ),
				'update_item' => __( 'Update '.$taxonomy->tax_single ),
				'add_new_item' => __( 'Add New '.$taxonomy->tax_single ),
				'new_item_name' => __( 'New '.$taxonomy->tax_single.' Name' ),
				'menu_name' => __( $taxonomy->tax_single )
			);

			if( isset($taxonomy->hierarchical) ) {
				$tax_hierarchical = $taxonomy->hierarchical;
			} else {
				$tax_hierarchical = true;
			}

			if( isset($taxonomy->public) && is_bool($taxonomy->public) ) {
				$tax_public = $taxonomy->public;
			} else {
				$tax_public = true;
			}

			if( isset($taxonomy->show_ui) && is_bool($taxonomy->show_ui) ) {
				$tax_show_ui = $taxonomy->show_ui;
			} else {
				$tax_show_ui = $tax_public;
			}

			if( isset($taxonomy->show_in_menu) && is_bool($taxonomy->show_in_menu) ) {
				$tax_show_in_menu = $taxonomy->show_in_menu;
			} else {
				$tax_show_in_menu = $tax_show_ui;
			}

			if( isset($taxonomy->show_in_nav_menus) && is_bool($taxonomy->show_in_nav_menus) ) {
				$tax_show_in_nav_menus = $taxonomy->show_in_nav_menus;
			} else {
				$tax_show_in_nav_menus = $tax_public;
			}

			if( isset($taxonomy->show_tagcloud) && is_bool($taxonomy->show_tagcloud) ) {
				$tax_show_tagcloud = $taxonomy->show_tagcloud;
			} else {
				$tax_show_tagcloud = $tax_show_ui;
			}

			if( isset($taxonomy->show_in_quick_edit) && is_bool($taxonomy->show_in_quick_edit) ) {
				$tax_show_in_quick_edit = $taxonomy->show_in_quick_edit;
			} else {
				$tax_show_in_quick_edit = $tax_show_ui;
			}

			if( isset($taxonomy->show_admin_column) && is_bool($taxonomy->show_admin_column) ) {
				$tax_show_admin_column = $taxonomy->show_admin_column;
			} else {
				$tax_show_admin_column = false;
			}

			if( isset($taxonomy->description) ) {
				$tax_description = $taxonomy->description;
			} else {
				$tax_description = '';
			}

			if( isset($taxonomy->query_var) ) {
				$tax_query_var = $taxonomy->query_var;
			} else {
				$tax_query_var = true;
			}

			if( isset($taxonomy->sort) ) {
				$tax_sort = $taxonomy->sort;
			} else {
				$tax_sort = '';
			}

			register_taxonomy($tax_name, array($post_name), array(
				'public' => $tax_public,
				'hierarchical' => $tax_hierarchical,
				'labels' => $tax_labels,
				'show_ui' => $tax_show_ui,
				'show_in_menu' => $tax_show_in_menu,
				'show_in_nav_menus' => $tax_show_in_nav_menus,
				'show_tagcloud' => $tax_show_tagcloud,
				'show_in_quick_edit' => $tax_show_in_quick_edit,
				'show_admin_column' => $tax_show_admin_column,
				'description' => $tax_description,
				'query_var' => $tax_query_var,
				'rewrite' => array( 'slug' => $taxonomy->slug ),
				'sort' => $tax_sort
			));
	  }
	}


	function get_settings_json()
	{
		$url = dirname(__FILE__) . '/settings.json';
		$json = file_get_contents($url);
		$json = preg_replace("/\/\*(?s).*?\*\//", "", $json);
		$json = json_decode($json);

		return $json;
	}


	public function json_error() {
		?>
		<div class="error">
			<p><?php _e( 'Settings.json in plugins/cptts is invalid.', 'themeName-admin' ); ?></p>
		</div>
		<?php
	}

}

new Cptts();

endif; // class_exists check

include('shortcodes.php');

