<?php


if( !class_exists('CpttsShortcodes') ):

class CpttsShortcodes {

	private $shortcodes = array(
		'button_func' => 'button',
		'link_func' => 'link',
		'custom_list' => 'ul',
		'custom_list_li' => 'li',
		'bagde_wrapper_func' => 'badge_wrapper',
		'badge' => 'badge',
		'person' => 'person',
		'image_wrapper_func' => 'image_wrapper',
		'color' => 'color',
		'plugin_box' => 'plugin_box',
		'url_wrapper' => 'url',
		's_func' => 's',
		'contact_person' => 'contact_person'
	);


	function __construct()
	{
		add_action( 'init', array( $this, 'create_shorcodes' ) );
	}

	public function create_shorcodes()
	{
		foreach( $this->shortcodes as $func => $name ) {
			add_shortcode($name, array($this, $func));
		}
	}

	function button_func($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'href' => '#',
			'color' => __('Color', 'cptts'),
			'popup' => ' '
		), $atts));

		return "<a href='$href' class='btn btn-$color $popup'>$content</a>";
	}

	function link_func($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'href' => '',
			'color' => __('Color', 'cptts'),
		), $atts));

		if($href){
			$html = "<a href='$href' ";
		}else{
			$html = "<span ";
		}

		$html .= " class='link link-$color'>$content";

		if($href){
			$html .= "</a>";
		}else{
			$html .= "</span>";
		}

		return $html;
	}

	function custom_list($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'type' => ''
		), $atts));

		return "<ul class='list-$type'>".do_shortcode($content)."</ul>";
	}

	function custom_list_li($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'number' => ''
		), $atts));

		$html = "<li>";

		if($number){
			$html .= "<span class='number'>$number</span>";
		}

		$html .= do_shortcode($content)."</li>";

		return $html;
	}

	function bagde_wrapper_func($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'align' => 'center',
		), $atts));
		return "<div class='badge-wrapper $align'>".do_shortcode($content)."</div>";
	}

	function badge($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'color' => __('Color', 'cptts'),
		), $atts));
		return "<span class='badge' style='background-color: $color'>".do_shortcode($content)."</span>";
	}

	function person($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'name' => __('Name', 'cptts'),
			'title' => __('Title', 'cptts')
		), $atts));
		return "<div class='person-caption'><h5>$name</h5><p><em>$title</em></p></div>";
	}

	function image_wrapper_func($atts, $content = null)
	{
		return "<div class='image-wrapper'>".do_shortcode($content)."</div>";
	}

	function color($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'color' => __('Color', 'cptts'),
		), $atts));
		return "<span class='color-text $color'>".do_shortcode($content)."</span>";
	}

	function plugin_box($atts, $content = null)
	{
		return "<div class='plugin-box'>".do_shortcode($content)."</div>";
	}

	function url_wrapper($atts, $content = null)
	{
		return "<span class='url'>".do_shortcode($content)."</span>";
	}
	function s_func($atts, $content = null)
	{
		return "<span>".do_shortcode($content)."</span>";
	}
	function contact_person($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'name' => __('Name', 'cptts'),
			'email' => __('Email', 'cptts'),
			'phone' => __('Phone', 'cptts')
		), $atts));
		return "<div class='person-contact'>$content<h4>$name</h4><p><a href='mailto:$email' class='link link-red'>$email</a></p><p>$phone</p></div>";
	}
}

new CpttsShortcodes();

endif; // class_exists check
