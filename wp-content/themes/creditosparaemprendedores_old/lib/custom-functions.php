<?php 

function localize_scripts() {
	$params = array(
		"siteUrl" => home_url(),
		"templateUrl" => get_template_directory_uri(),
		"ajaxUrl" => site_url()."/wp-admin/admin-ajax.php"
		);
	wp_localize_script('script', 'SiteVars', $params);
}