<?php
/**
* The Banner template for theme.
*
* Displays all of the <head> section and page header
*
* @package WordPress
* @subpackage CreditosParaEmprendedores
* @since CreditosParaEmprendedores 1.0
*/
?>

<section class="banner-section" style="background-image: url(<?php echo wp_get_attachment_image_src(get_field('banner_image', 'options'), 'full')[0]; ?>);">
	<div class="container">
		<div class="content">
			<p><?php the_field('banner_text', 'options'); ?></p>
			<a class="btn" href="<?php the_field('banner_button_url', 'options'); ?>"><?php the_field('banner_button_text', 'options'); ?></a>
		</div>
	</div>
</section>