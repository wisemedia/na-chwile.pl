<?php
/**
 * Loop for home page.
 *
 * @package WordPress
 * @subpackage CreditosParaEmprendedores
 * @since CreditosParaEmprendedores 1.0
 */
?>
<section class="posts">
	<?php while (have_posts()) : the_post(); ?>
		<article class="post">
			<div class="gfx" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
				<?php the_post_thumbnail('post-thumb-category'); ?>
			</div>
			<div class="content">
				<div class="post-entry">
					<small class="post-info"><span class="author"><?php the_author(); ?></span><span class="date"><?php echo get_the_date(); ?></span></small>
					<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php the_excerpt(); ?>
				</div>
				<a href="<?php the_permalink(); ?>" class="btn-simple"><?php _e('Read more'); ?></a>
			</div>
		</article>
	<?php endwhile; ?>
</section>