<?php
/**
 * The single post page template.
 *
 *
 * @package WordPress
 * @subpackage CreditosParaEmprendedores
 * @since CreditosParaEmprendedores 1.0
 */

get_header(); the_post(); ?>
	<?php get_template_part('banner', 'index'); ?>
	<main class="content">
		<div class="container">
			<article class="post">
				<div class="gfx">
					<?php the_post_thumbnail('post-thumb'); ?>
				</div>
				<div class="content">
					<div class="post-entry">
						<small class="post-info"><span class="author"><?php the_author(); ?></span><span class="date"><?php echo get_the_date(); ?></span></small>
						<h1 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
						<div class="cms-content">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</article>
			
			<?php get_template_part('sidebar') ?>

		</div>
	</main>

<?php get_footer(); ?>