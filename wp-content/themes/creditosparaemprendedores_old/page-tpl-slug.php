<?php
/**
 * Template Name: Template Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage CreditosParaEmprendedores
 * @since CreditosParaEmprendedores 1.0
 */

get_header(); the_post(); ?>

	<main class="content full-width">
		<h1><?php the_title(); ?></h1>

		<?php the_content(); ?>

		<?php 
		$queryArgs = array(
			'post_type' => 'post_type',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'people',
					'field' => 'slug',
					'terms' => 'bob'
					)
				),
			'meta_query' => array(
				array(
					'key' => 'color',
					'value' => 'blue',
					'compare' => 'NOT LIKE'
					)
				)
			);
		query_posts($queryArgs);
		?>
		
		<?php while(have_posts()) : the_post(); ?>

			<article>
				<header>
					<h2><?php the_title(); ?></h2>
				</header>
				<?php the_content(); ?>
			</article>
			
		<?php endwhile; wp_reset_query(); ?>
		
	</main>

<?php get_footer(); ?>