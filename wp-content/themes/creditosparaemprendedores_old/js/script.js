var $ = jQuery.noConflict();

var Project = {

	init: function()
	{
		$('html').removeClass('no-js');

		$('.btn-menu').on('click', function(e){
			e.preventDefault();
			$(this).toggleClass('active');
		})
	}

}

$(document).ready(function() {
	Project.init();
});
