<?php
/**
 * The footer for theme.
 *
 *
 * @package WordPress
 * @subpackage CreditosParaEmprendedores
 * @since CreditosParaEmprendedores 1.0
 */
?>
		<footer class="main">
			<div class="container">
				<p class="logo-wrapper"><a href="<?php echo get_home_url(); ?>" class="logo"><?php echo wp_get_attachment_image(get_field('logo', 'options'), 'logo-small'); ?></a></p>
				<nav>
					<?php wp_nav_menu(array("theme_location" => 'footer', 'container' => false)); ?>
				</nav>
			</div>
		</footer>
	</div> <!-- /#page --> 
<?php wp_footer(); ?>
</body>
</html>