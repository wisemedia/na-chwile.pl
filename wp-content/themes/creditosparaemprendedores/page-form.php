<?php
/*
	Template Name: Form Page Template
 */
if(!($_POST['loan_period'] && $_POST['loan_amount'] && $_POST['loan_sum'] && $_POST['loan_cost'] && $_POST['loan_apr'] && $_POST['loan_repayment'])){
	header('Location: '.get_home_url());
}
get_header(); ?>
	<div class="form-wrapper">
		<div class="container">
			<div class="row">
				<div class="col content-wrapper">
					<div class="box">
						<div class="form-steps-wrapper">
							<h1><?php the_field('heading'); ?></h1>
							<div class="progress-wrapper">
								<div class="steps-wrapper">
									<div class="active">
										<?php if(get_field('step_1_label')) : ?><span><?php the_field('step_1_label'); ?></span><?php endif; ?>
										<p><?php the_field('step_1_title'); ?></p>
									</div>
									<div>
										<?php if(get_field('step_2_label')) : ?><span><?php the_field('step_2_label'); ?></span><?php endif; ?>
										<p><?php the_field('step_2_title'); ?></p>
									</div>
									<div>
										<?php if(get_field('step_3_label')) : ?><span><?php the_field('step_3_label'); ?></span><?php endif; ?>
										<p><?php the_field('step_3_title'); ?></p>
									</div>
								</div>
								<div class="steps-indicator">
									<span></span>
								</div>
								<?php if(get_field('step_1_description')) : ?><p class="step-description step-0"><b class="form-progress">0%</b> <?php the_field('step_1_description'); ?></p><?php endif; ?>
								<?php if(get_field('step_2_description')) : ?><p class="step-description step-1"><b class="form-progress">0%</b> <?php the_field('step_2_description'); ?></p><?php endif; ?>
								<?php if(get_field('step_3_description')) : ?><p class="step-description step-2"><b class="form-progress">0%</b> <?php the_field('step_3_description'); ?></p><?php endif; ?>
								<?php if(get_field('thank_you_description')) : ?><p class="step-description step-3"><b class="form-progress">0%</b> <?php the_field('thank_you_description'); ?></p><?php endif; ?>
							</div>
						</div>
						<form id="loan-form" action="#" novalidate>
							<!-- <input type="hidden" name="email" value="<?php echo $_POST['email']; ?>"> -->
							<input type="hidden" name="loan_amount" value="<?php echo $_POST['loan_amount']; ?>">
							<input type="hidden" name="loan_period" value="<?php echo $_POST['loan_period']; ?>">
							<input type="hidden" name="loan_cost" value="<?php echo $_POST['loan_cost']; ?>">
							<?php
								$fields = get_form_fields()->fields;
								for ($i=0; $i < count($fields); $i++) { 
									if ($fields[$i]->key == 'nationality'){
										$nationality = $fields[$i]->values;
									}
									if ($fields[$i]->key == 'occupation_type'){
										$occupation_type = $fields[$i]->values;
									}
									if ($fields[$i]->key == 'remuneration_date'){
										$remuneration_date = $fields[$i]->values;
									}
									if ($fields[$i]->key == 'home_ownership_type'){
										$home_ownership_type = $fields[$i]->values;
									}
									if ($fields[$i]->key == 'gender'){
										$gender = $fields[$i]->values;
									}
								}
							?>
							<div id="step-0" class="step">
								<fieldset>
									<h2><?php pll_e('Personal Data'); ?></h2>
									<div class="form-item">
										<label class="label" for="first_name"><?php pll_e('Name'); ?></label>
										<input type="text" name="first_name" id="first_name"/>
										<small><?php pll_e('Introduce tu nombre tal y como aparece en tu DNI/NIE'); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="last_name"><?php pll_e('Last Name', 'creditocajeropremium'); ?></label>
										<input type="text" name="last_name" id="last_name" />
										<small><?php pll_e('Introduce tu primer apellido tal y como aparece en tu DNI/NIE'); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="surname"><?php pll_e('Surname', 'creditocajeropremium'); ?></label>
										<input type="text" name="surname" id="surname" class="optional"/>
										<small><?php pll_e('Introduce tu segundo apellido tal y como aparece en tu DNI/NIE'); ?></small>
									</div>
									<?php if($gender) : ?>
									<div class="form-item">
										<label class="label"><?php pll_e('Gender'); ?></label>
										<?php for ($i=0; $i < count($gender); $i++) : ?>
										<label><input type="radio" name="gender" value="<?php echo $gender[$i]->key; ?>" <?php if(!$i) : ?>checked<?php endif; ?> /><?php echo $gender[$i]->name; ?></label>
										<?php endfor; ?>
										<!-- <small><?php pll_e('Please choose your gender'); ?></small> -->
									</div>
									<?php endif; ?>
									<div class="form-item">
										<label class="label" for="birth_date"><?php pll_e('Birthdate'); ?></label>
										<input type="text" name="birth_date" id="birth_date" class="datepicker birthdate"/>
										<small><?php pll_e('Selecciona tu fecha de nacimiento. Tienes que ser mayor de edad para solicitar un préstamo.'); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="personal_code"><?php pll_e('DNI / NIE'); ?></label>
										<input type="text" name="personal_code" id="personal_code" />
										<small><?php pll_e('Introduce el número tal y como aparece en tu DNI/NIE.'); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="phone_number_without_area_code"><?php pll_e('Phone number'); ?></label>
										<input type="text" name="phone_number_without_area_code" id="phone_number_without_area_code" />
										<small><?php pll_e('Enter the number without spaces. We will send you a confirmation code to verify the process.'); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="email"><?php pll_e('E-mail'); ?></label>
										<input type="email" name="email" id="email" />
										<small><?php pll_e('Enter your e-mail address.'); ?></small>
									</div>
									<?php if($nationality) : ?>
									<div class="form-item">
										<label class="label" for="nationality"><?php pll_e('Nationality'); ?></label>
										<select name="nationality" id="nationality">
											<?php for ($i=0; $i < count($nationality); $i++) : ?>
											<option value="<?php echo $nationality[$i]->name; ?>"><?php echo $nationality[$i]->key; ?></option>
											<?php endfor; ?>
										</select>
										<small class="error-msg"><?php pll_e('Please select one option.'); ?></small>
									</div>
									<?php endif; ?>
								</fieldset>
								<fieldset>
									<h2><?php pll_e('Creating account'); ?></h2>
									<div class="form-item vertical">
										<label><input type="checkbox" name="accept-privacy-policy" value="true"/><a href="<?php echo get_permalink(get_page_by_path('politica-de-privacidad'))?>" target="_blank"><?php pll_e('I accept the Privacy Policy.'); ?></a></label>
										<small class="error-msg"><?php pll_e('Please check this field.'); ?></small>
									</div>
								</fieldset>
								<div class="btn-wrapper">
									<button type="button" class="btn btn-primary" data-step="1"><?php pll_e('Next step to get a loan'); ?></button>
								</div>
							</div>
							<div id="step-1" class="step">
								<fieldset>
									<h2><?php pll_e('Address details'); ?></h2>
									<div class="form-item">
										<label class="label" for="street"><?php pll_e('Street address'); ?></label>
										<input type="text" name="street" id="street" />
										<small><?php pll_e('Insert your living address street'); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="house_number"><?php pll_e('House number'); ?></label>
										<input type="text" name="house_number" id="house_number" />
										<small><?php pll_e('Introduce la información adicional de tu dirección.'); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="house_number_appendix"><?php pll_e('House number apppendix'); ?></label>
										<input type="text" name="house_number_appendix" id="house_number_appendix" class="optional"/>
										<small><?php pll_e("If your current living address has a appendix then please enter it here. If you don't have appendix then leave this field blank"); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="zip_code"><?php pll_e('Postal code'); ?></label>
										<input type="text" name="zip_code" id="zip_code" />
										<small><?php pll_e('Introduce el código postal de tu población'); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="city"><?php pll_e('City'); ?></label>
										<input type="text" name="city" id="city" />
										<small><?php pll_e('Insert your current city'); ?></small>
									</div>
									<?php if($home_ownership_type) : ?>
									<div class="form-item vertical">
										<label class="label"><?php pll_e('Type of property'); ?></label>
										<?php for ($i=0; $i < count($home_ownership_type); $i++) : ?>
										<label><input type="radio" name="home_ownership_type" value="<?php echo $home_ownership_type[$i]->key; ?>" <?php if(!$i) : ?>checked<?php endif; ?> /><?php echo $home_ownership_type[$i]->name; ?></label>
										<?php endfor; ?>
										<small class="error-msg"><?php pll_e('Please check one option.'); ?></small>
										<small><?php pll_e('Selecciona el estado de tu vivienda actual'); ?></small>
									</div>
									<?php endif; ?>
								</fieldset>
								<div class="btn-wrapper">
									<button type="button" class="btn btn-primary" data-step="2"><?php pll_e('Next step to get a loan'); ?></button>
								</div>
							</div>
							<div id="step-2" class="step">			
								<fieldset>
									<h2><?php pll_e('Employment Info'); ?></h2>
									<div class="form-item">
										<label class="label" for="bank_account_number_without_prefix"><?php pll_e('Bank account'); ?></label>
										<input type="text" name="bank_account_number_without_prefix" id="bank_account_number_without_prefix" />
										<small><?php pll_e('Insert your bank account number without prefix.'); ?>.</small>
									</div>
									<?php if($occupation_type) : ?>
									<div class="form-item">
										<label class="label" for="occupation_type"><?php pll_e('Occupation Type'); ?></label>
										<select name="occupation_type" id="occupation_type">
											<?php for ($i=0; $i < count($occupation_type); $i++) : ?>
											<option value="<?php echo $occupation_type[$i]->key; ?>"><?php echo $occupation_type[$i]->name; ?></option>
											<?php endfor; ?>
										</select>
										<small><?php pll_e('Selecciona tu situación laboral actual'); ?></small>
									</div>
									<?php endif; ?>
									<div class="form-item">
										<label class="label" for="current_employment_since"><?php pll_e('Current employment since'); ?></label>
										<input type="text" name="current_employment_since" id="current_employment_since" class="datepicker employment" />
										<small><?php pll_e('Selecciona la fecha de inicio de tu situación laboral actual'); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="net_income_localized"><?php pll_e('Netto income'); ?></label>
										<input type="text" name="net_income_localized" id="net_income_localized" />
										<small><?php pll_e('Insert your netto income.'); ?></small>
									</div>
									<div class="form-item">
										<label class="label" for="dependant_count"><?php pll_e('Dependent count'); ?></label>
										<input type="text" name="dependant_count" id="dependant_count" />
										<small><?php pll_e('Indica cuántas personas dependen de ti'); ?></small>
									</div>
									<?php if($remuneration_date) : ?>
									<div class="form-item">
										<label class="label" for="remuneration_date"><?php pll_e('Renumeration date'); ?></label>
										<select name="remuneration_date" id="remuneration_date">
											<?php for ($i=0; $i < count($remuneration_date); $i++) : ?>
											<option value="<?php echo $remuneration_date[$i]->name; ?>"><?php echo $remuneration_date[$i]->key; ?></option>
											<?php endfor; ?>
										</select>
										<small><?php pll_e('Indica el día del mes en que tienes previsto recibir tu próximo ingreso o en el que acostumbras a recibir tu ingreso habitual'); ?></small>
									</div>
									<?php endif; ?>
								</fieldset>
								<div class="btn-wrapper">
									<button type="submit" class="btn btn-primary" data-step="3"><?php pll_e('Send application'); ?></button>
								</div>
							</div>
							<div class="loader"></div>
						</form>
					</div>
				</div>
				<aside class="col">
					<div class="box">
						<h3><?php the_field('summary_title'); ?></h3>
						<table>
							<tbody>
								<tr>
									<td><?php the_field('summary_loan_label'); ?>:</td>
									<td>
										<span><?php echo $_POST['loan_amount']; ?> €</span>
									</td>
								</tr>
								<tr>
									<td><?php the_field('summary_cost_label'); ?>:</td>
									<td>
										<span><?php echo $_POST['loan_cost']; ?> €</span>
									</td>
								</tr>
								<tr>
									<td><?php the_field('summary_sum_label'); ?>:</td>
									<td>
										<span><?php echo $_POST['loan_sum']; ?> €</span>
										<small><?php pll_e('APR'); ?> <?php echo $_POST['loan_apr']; ?> %</small>
									</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td><?php the_field('summary_repayment_label'); ?>:</td>
									<td>
										<span><?php echo $_POST['loan_repayment']; ?></span>
										<small><span class="display-period"><?php echo $_POST['loan_period']; ?></span> <?php pll_e('days', 'creditocajeropremium'); ?>!</small>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
					<?php if(get_field('support_box_heading')) : ?>
					<div class="box support">
						<p><?php the_field('support_box_heading'); ?></p>
						<?php 
							$contact_phone = get_field('contact_phone', 'options'); 
							$contact_email = get_field('contact_email', 'options'); 
						?>
						<?php if($contact_phone || $contact_email): ?>
						<ul class="contact-links">
							<?php if($contact_email): ?>
							<li><a href="mailto:<?php echo $contact_email; ?>"><?php echo $contact_email; ?></a></li>
							<?php endif; ?>

							<?php if($contact_phone): ?>
							<li><a href="tel:<?php echo $contact_phone; ?>"><?php echo $contact_phone; ?></a></li>
							<?php endif; ?>
						</ul>
						<?php endif; ?>
					</div>
					<?php endif; ?>
				</aside>
			</div>
		</div>
	</div>
	<div class="thank-you-wrapper">
		<div class="container">
			<div class="box thank-you accepted" <?php if(get_field('accepted_icon')) { echo 'style="background-image: url(' . get_field('accepted_icon') .');"'; }; ?>>
				<div class="content">
					<span class="fa fa-check"></span>
					<h2><?php the_field('accepted_heading'); ?></h2>
					<?php the_field('accepted_content'); ?>
					<a href="<?php echo get_home_url(); ?>" class="btn btn-primary"><?php pll_e('OK'); ?></a>
				</div>
			</div>
			<div class="box thank-you rejected" <?php if(get_field('rejected_icon')) { echo 'style="background-image: url(' . get_field('accepted_icon') .');"'; }; ?>>
				<div class="content">
					<span class="fa fa-frown-o"></span>
					<h2><?php the_field('rejected_heading'); ?></h2>
					<?php the_field('rejected_content'); ?>
					<a href="<?php echo get_home_url(); ?>" class="btn btn-primary"><?php pll_e('OK'); ?></a>
				</div>
			</div>
			<div class="box thank-you api-error" <?php if(get_field('error_icon')) { echo 'style="background-image: url(' . get_field('accepted_icon') .');"'; }; ?>>
				<div class="content">
					<span class="fa fa-bolt"></span>
					<h2><?php the_field('error_heading'); ?></h2>
					<?php the_field('error_content'); ?>
					<a href="<?php echo get_home_url(); ?>" class="btn btn-primary"><?php pll_e('OK'); ?></a>
				</div>
			</div>
		</div>
	</div>

<?php get_footer();