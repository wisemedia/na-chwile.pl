<?php
//**************************************************************************** */
//
//  1. Init
//  2. Navigation
//  3. Widget Areas
//  4. Thumbnails
//  5. Core functions
//  6. Custom functions
//
//**************************************************************************** */

/* * ******************************** Init *********************************** */

include_once 'lib/init.php';

/* * ****************************** Navigation ******************************* */

include_once 'lib/nav.php';

/* * ****************************** Widget Areas ***************************** */

include_once 'lib/widget-areas.php';

/* * ****************************** Thumbnails ******************************* */

include_once 'lib/thumbnails.php';

/* * ************************* Core Functions ******************************** */

include_once 'lib/core-functions.php';

/* * ************************ Custom Functions ******************************* */

include_once 'lib/custom-functions.php';

/* * ************************** Code Snippets ******************************** */

foreach( glob(get_template_directory() . '/lib/snippets/*', GLOB_ONLYDIR) as $dir ) {
	$files = glob($dir . '/*.php');

	foreach( $files as $file ) {
		$headers = get_file_data($file, array('type' => 'Type'));

		if( ! empty($headers['type']) && 'snippet' == $headers['type'] )
			include_once $file;
	}
}

function be_menu_item_classes( $classes, $item, $args ) {
	if( 'header' !== $args->theme_location )
		return $classes;
	if( ( is_singular( 'post' ) || is_category() || is_tag() ) && 'Blog' == $item->title )
		$classes[] = 'current-menu-item';
		
	return array_unique( $classes );
}
add_filter( 'nav_menu_css_class', 'be_menu_item_classes', 10, 3 );

/**
 * Filter the "read more" excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return sprintf( '... <a class="link-simple" href="%1$s">%2$s</a>',
        get_permalink( get_the_ID() ),
        __( 'Read more', 'textdomain' )
    );
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


// Custom code
class FF_Calculator_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'ff_calculator_widget', // Base ID
			esc_html__( 'FF Calculator Widget', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'Renders sliders with calculator', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		?>
			<form id="slider-form" class="loading" method="post" action="<?php echo get_permalink(get_page_by_path('form'))?>" novalidate>
				<input type="hidden" name="loan_period" value=""/>
				<input type="hidden" name="loan_amount" value=""/>
				<input type="hidden" name="loan_cost" value=""/>
				<input type="hidden" name="loan_sum" value=""/>
				<input type="hidden" name="loan_repayment" value=""/>
				<input type="hidden" name="loan_apr" value=""/>
				<div class="loader"></div>
				<div class="calculator-wrapper">
					<h2><?php pll_e('What amount of money do you need?'); ?></h2>
					<div class="slider-wrapper">
						<button type="button" class="slider-less"></button>
						<div id="loan_amount" class="slider" data-min="" data-max="" data-step="" data-default="">
							<div class="ui-slider-handle"><span class="slider-value"></span></div>
						</div>
						<button type="button" class="slider-more"></button>
					</div>
					<p>
						<span><span class="display-min-amount"></span> <span class="display-currency"></span></span>
						<span><span class="display-max-amount"></span> <span class="display-currency"></span></span>
					</p>
					<h2><?php pll_e('For how long?'); ?></h2>
					<div class="slider-wrapper">
						<button type="button" class="slider-less"></button>
						<div id="loan_period" class="slider" data-min="" data-max="" data-step="" data-default="">
							<div class="ui-slider-handle"><span class="slider-value"></span></div>
						</div>
						<button type="button" class="slider-more"></button>
					</div>
					<p>
						<span><span class="display-min-period"></span> <span class="display-days"></span></span>
						<span><span class="display-max-period"></span> <span class="display-days"></span></span>
					</p>
					<div class="summary">
						<table>
							<tbody>
								<tr>
									<td><?php pll_e('Loan amount'); ?></td>
									<td><span class="loan-amount"></span> <span class="display-currency"></span></td>
								</tr>
								<tr>
									<td><?php pll_e('Loan cost'); ?></td>
									<td><span class="loan-cost"></span> <span class="display-currency"></span></td>
								</tr>
								<tr>
									<td><?php pll_e('Loan sum'); ?></td>
									<td><span class="loan-sum"></span> <span class="display-currency"></span></td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2"><span><?php pll_e('APR'); ?>&nbsp;</span><span><span class="display-apr"></span>%</span></td>
								</tr>
							</tfoot>
						</table>
					</div>
					<div class="btn-wrapper">
						<button type="button" class="btn"><?php pll_e('Take a loan'); ?> &raquo;</button>
					</div>
				</div>
			</form>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		// $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
		?>
		<!-- <p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p> -->
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

}
// register Foo_Widget widget
function register_ff_calculator_widget() {
    register_widget( 'FF_Calculator_Widget' );
}
add_action( 'widgets_init', 'register_ff_calculator_widget' );


function authorize() {
	$api_key = "61bcd038c9d2283be68af0adf83f6b09";
	$secret_key = "be79da656ba63ebf3b05f39242251a37";
	$timestamp = time();
	$signature = $api_key . $timestamp . $secret_key;

	return $auth_data = array(
		api_url => 'https://staging.creditocajero.es/api/v2',  // test
		// api_url => 'https://creditocajero.es/api/v2',  // live
		api_key => $api_key,
		timestamp => $timestamp,
		signature => base64_encode(md5($signature,true)),
		flow => 'affiliate_api_v2_google'
	);
}

add_action( 'wp_ajax_nopriv_send_application_loan', 'send_application_loan' );
add_action( 'wp_ajax_send_application_loan', 'send_application_loan' );
add_action( 'wp_ajax_nopriv_get_parameters', 'get_parameters' );
add_action( 'wp_ajax_get_parameters', 'get_parameters' );

function get_parameters() {
	$authorize_data = authorize();
	$api_url = $authorize_data['api_url'].'/products/short_term/price_list?api_key='.$authorize_data['api_key'].'&timestamp='.$authorize_data['timestamp'].'&signature='.urlencode($authorize_data['signature']).'&flow='.$authorize_data['flow'];

	$remote = wp_remote_post(
		$api_url,
		array(
			'headers'   => array('Content-Type' => 'application/json; charset=utf-8'),
			'method'    => 'GET',
			'timeout'	=> 45
		)
	);

	echo json_encode($remote);
	wp_die();
}

function send_application_loan() {
	$authorize_data = authorize();

	$affiliate_identifier = 'affiliate_api / '.$authorize_data['api_key'];
	if(isset($_COOKIE['source'])) {
		$affiliate_identifier .= ' / ' . $_COOKIE['source'];
	}

	$api_url = $authorize_data['api_url'].'/loans';

	$default_data = array(
		'api_key' => $authorize_data['api_key'],
		'signature' => $authorize_data['signature'],
		'timestamp' => $authorize_data['timestamp'],
		'flow' => $authorize_data['flow'],
		'customer' => array(
			'first_name' => '',
			'last_name' => '',
			'surname' => '',
			'email' => '',
			'phone_number_without_area_code' => '',
			'birth_date' => '',
			'personal_code' => '',
			'gender' => '',
			'street' => '',
			'city' => '',
			'house_number' => '',
			'house_number_appendix' => '',
			'zip_code' => '',
			'home_ownership_type' => '',
			'source' => $affiliate_identifier,
			'base_amount' => '',
			'period' => '',
			'loan_fee' => '',
			'occupation_type' => '',
			'current_employment_since' => '',
			'net_income_localized' => '',
			'dependant_count' => '',
			'remuneration_date' => '',
			'bank_account_number_without_prefix' => '',
			'loan_product_package_checkbox' => ''
		)
	);

	$post_data = array(
		'api_key' => $authorize_data['api_key'],
		'signature' => $authorize_data['signature'],
		'timestamp' => $authorize_data['timestamp'],
		'flow' => $authorize_data['flow'],
		'customer' => array(
			'first_name' => trim($_POST['first_name']),
			'last_name' => trim($_POST['last_name']),
			'surname' => trim($_POST['surname']),
			'email' => trim($_POST['email']),
			'phone_number_without_area_code' => trim($_POST['phone_number_without_area_code']),
			'birth_date' => trim($_POST['birth_date']),
			'personal_code' => trim($_POST['personal_code']),
			'gender' => trim($_POST['gender']),
			'street' => trim($_POST['street']),
			'city' => trim($_POST['city']),
			'house_number' => trim($_POST['house_number']),
			'house_number_appendix' => trim($_POST['house_number_appendix']),
			'zip_code' => trim($_POST['zip_code']),
			'home_ownership_type' => trim($_POST['home_ownership_type']),
			'source' => $affiliate_identifier,
			'base_amount' => trim($_POST['loan_amount']),
			'period' => trim($_POST['loan_period']),
			'loan_fee' => trim($_POST['loan_cost']),
			'occupation_type' => trim($_POST['occupation_type']),
			'current_employment_since' => trim($_POST['current_employment_since']),
			'net_income_localized' => trim($_POST['net_income_localized']),
			'dependant_count' => trim($_POST['dependant_count']),
			'remuneration_date' => trim($_POST['remuneration_date']),
			'bank_account_number_without_prefix' => trim($_POST['bank_account_number_without_prefix']),
			'loan_product_package_checkbox' => 'google'
		)
	);

	$data_to_post = array_merge( $default_data, $post_data );

	$remote = wp_remote_post(
		$api_url,
		array(
			'headers'   => array('Content-Type' => 'application/json; charset=utf-8'),
			'body'      => json_encode($data_to_post),
			'method'    => 'POST',
			'timeout'	=> 45
		)
	);

	$file = get_template_directory() . "/my-logs.log";
	$time = date("F jS Y, H:i", time() + 60*60);
	$message = json_encode($data_to_post) . PHP_EOL . PHP_EOL;

	error_log($message, 3, $file);

	echo json_encode($remote);
	wp_die();
}

function get_form_fields() {
	$authorize_data = authorize();

	$api_url = $authorize_data['api_url'].'/loans/new?api_key='.$authorize_data['api_key'].'&timestamp='.$authorize_data['timestamp'].'&signature='.urlencode($authorize_data['signature']).'&locale=es&flow='.$authorize_data['flow'];

	$remote = wp_remote_get(
		$api_url,
		array(
			'headers'   => array('Content-Type' => 'application/json; charset=utf-8'),
			'method'    => 'GET',
			'timeout'	=> 100
		)
	);

	// var_dump($remote['body']);

	return json_decode($remote['body']);
}

if(function_exists('pll_register_string')) {
	pll_register_string('Slider Title', 'What amount of money do you need?', 'creditosparaemprendedores');
	pll_register_string('Slider Title', 'For how long?', 'creditosparaemprendedores');
	pll_register_string('Slider Label', 'Loan amount', 'creditosparaemprendedores');
	pll_register_string('Slider Label', 'Loan cost', 'creditosparaemprendedores');
	pll_register_string('Slider Label', 'Loan sum', 'creditosparaemprendedores');
	pll_register_string('Slider Button', 'Take a loan', 'creditosparaemprendedores');
	pll_register_string('Fieldset Title', 'Personal Data', 'creditosparaemprendedores');
	pll_register_string('Fieldset Title', 'Creating account', 'creditosparaemprendedores');
	pll_register_string('Fieldset Title', 'Address details', 'creditosparaemprendedores');
	pll_register_string('Fieldset Title', 'Employment Info', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Name', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Surname', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Last Name', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Gender', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Birthdate', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'DNI / NIE', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Phone number', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Nationality', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'E-mail', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Password', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'I accept the Privacy Policy.', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Street address', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'House number', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'House number apppendix', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Postal code', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'City', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Type of property', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Bank account', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Occupation Type', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Current employment since', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Netto income', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Dependent count', 'creditosparaemprendedores');
	pll_register_string('Form Labels', 'Renumeration date', 'creditosparaemprendedores');
	pll_register_string('Error Message', 'Please fill this field.', 'creditosparaemprendedores');
	pll_register_string('Error Message', 'Please check one option.', 'creditosparaemprendedores');
	pll_register_string('Error Message', 'Please provide validd phone number. We will send you the validation code.', 'creditosparaemprendedores');
	pll_register_string('Error Message', 'Please select one option.', 'creditosparaemprendedores');
	pll_register_string('Error Message', 'At least 6 characters.', 'creditosparaemprendedores');
	pll_register_string('Error Message', 'Please check this field.', 'creditosparaemprendedores');
	pll_register_string('Form Buttons', 'Next step to get a loan', 'creditosparaemprendedores');
	pll_register_string('Form Buttons', 'Send application', 'creditosparaemprendedores');
	pll_register_string('404 Error', 'This page doesn’t exist', 'creditosparaemprendedores');
	pll_register_string('Other', 'days', 'creditosparaemprendedores');
	pll_register_string('Other', 'Loading', 'creditosparaemprendedores');
	pll_register_string('Other', 'APR', 'creditosparaemprendedores');
	pll_register_string('Other', 'OK', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Introduce tu nombre tal y como aparece en tu DNI/NIE', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Introduce tu primer apellido tal y como aparece en tu DNI/NIE', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Introduce tu segundo apellido tal y como aparece en tu DNI/NIE', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Introduce el número tal y como aparece en tu DNI/NIE.', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Selecciona tu fecha de nacimiento. Tienes que ser mayor de edad para solicitar un préstamo.', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Please choose your gender', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Indica cuántas personas dependen de ti', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Selecciona tu situación laboral actual', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Selecciona la fecha de inicio de tu situación laboral actual', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Indica el día del mes en que tienes previsto recibir tu próximo ingreso o en el que acostumbras a recibir tu ingreso habitual', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Enter the number without spaces. We will send you a confirmation code to verify the process.', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Insert your living address street', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Introduce la información adicional de tu dirección.', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', "If your current living address has a appendix then please enter it here. If you don't have appendix then leave this field blank", 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Insert your current city', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Introduce el código postal de tu población', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Selecciona el estado de tu vivienda actual', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Insert your bank account number without prefix.', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Insert your netto income.', 'creditosparaemprendedores');
	pll_register_string('Form Field Tip', 'Enter your e-mail address.', 'creditosparaemprendedores');
}