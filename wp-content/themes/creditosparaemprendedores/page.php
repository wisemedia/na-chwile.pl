<?php
/**
 * The static page template.
 *
 *
 * @package WordPress
 * @subpackage CreditosParaEmprendedores
 * @since CreditosParaEmprendedores 1.0
 */

get_header(); the_post(); ?>

	<section class="main">
		<div class="container">
			<main class="content">
				<article>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<?php wp_link_pages(); ?>
				</article>
				<?php get_template_part('sidebar'); ?>
			</main>
		</div>
	</section>

<?php get_footer(); ?>