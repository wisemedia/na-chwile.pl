<?php
/**
 * Loop for home page.
 *
 * @package WordPress
 * @subpackage CreditosParaEmprendedores
 * @since CreditosParaEmprendedores 1.0
 */
?>
<section class="posts">
	<?php while (have_posts()) : the_post(); ?>
		<article class="post">
			<div class="gfx">
				<?php the_post_thumbnail('post-thumb-category'); ?>
			</div>
			<div class="content">
				<div class="post-entry">
					<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php the_excerpt(); ?>
				</div>
				<small class="post-info"><?php the_category(); ?><span class="date"><?php echo get_the_date(); ?></span></small>
			</div>
		</article>
	<?php endwhile; ?>
</section>