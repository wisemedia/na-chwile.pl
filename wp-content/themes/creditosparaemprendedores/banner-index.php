<?php
/**
* The Banner template for theme.
*
* Displays all of the <head> section and page header
*
* @package WordPress
* @subpackage CreditosParaEmprendedores
* @since CreditosParaEmprendedores 1.0
*/
?>

<section class="banner-section">
	<div class="container">
		<?php echo wp_get_attachment_image(get_field('banner_image', 'options'), 'banner'); ?>
	</div>
</section>