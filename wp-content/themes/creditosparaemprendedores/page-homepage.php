<?php
/*
 * Template Name: Homepage Template
 *
 * The template for displaying contact page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CreditocajeroPremium.es
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

	<?php
		$background_image_url = get_field('background_image');
		$background_color = get_field('background_color');
	?>
	<section class="hero-section" style="background-image: url('<?php echo $background_image_url; ?>'); background-color: <?php echo $background_color; ?>;">
		<div class="container">
			<div class="row justify-content-md-end">
				<div class="col-md-8 col-lg-7 col-xl-6">
					<div class="form-wrapper">
						<?php
							$main_heading = get_field('main_heading');
							$additional_heading = get_field('additional_heading');
						?>
						<?php if($main_heading || $additional_heading): ?>
						<h1><?php if($main_heading): ?><span class="highlight"><?php echo $main_heading; ?></span><?php endif; ?> <?php if($additional_heading): ?><span><?php echo $additional_heading; ?></span><?php endif; ?></h1>
						<?php endif; ?>
						<form id="slider-form" class="loading" method="post" action="<?php echo get_permalink(get_page_by_path('form'))?>" novalidate>
							<input type="hidden" name="loan_period" value=""/>
							<input type="hidden" name="loan_amount" value=""/>
							<input type="hidden" name="loan_cost" value=""/>
							<input type="hidden" name="loan_sum" value=""/>
							<input type="hidden" name="loan_repayment" value=""/>
							<input type="hidden" name="loan_apr" value=""/>
							<div class="loader">
								<div class="blobs">
									<div class="blob_left"></div>
									<div class="blob_right"></div>
									<p><?php pll_e('Loading', 'creditocajeropremium'); ?>...</p>
								</div>
							</div>
							<div class="calculator-wrapper">
								<div class="slider-wrapper">
									<p>
										<span><span class="display-min-amount"></span> <span class="display-currency"></span></span>
										<span><span class="display-max-amount"></span> <span class="display-currency"></span></span>
									</p>
									<div id="loan_amount" class="slider" data-min="50" data-max="300" data-step="5" data-default="120">
										<span class="slider-amount"><span class="slider-value">120</span> <span class="display-currency"></span></span>
									</div>
								</div>
								<div class="summary">
									<table>
										<tbody>
											<tr>
												<td><?php the_field('summary_loan_label'); ?>:</td>
												<td><span class="loan-amount"></span> <span class="display-currency"></span></td>
											</tr>
											<tr>
												<td><?php the_field('summary_cost_label'); ?>:</td>
												<td><span class="loan-cost"></span> <span class="display-currency"></span></td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<td><?php the_field('summary_sum_label'); ?>:</td>
												<td><span class="loan-sum"></span> <span class="display-currency"></span></td>
											</tr>
										</tfoot>
									</table>
									<div>
										<p><?php the_field('repayment_label'); ?>: <span class="loan-repayment"></span></p>
										<small><?php the_field('repayment_info'); ?> <b><span class="display-period"></span> <?php pll_e('days', 'creditocajeropremium'); ?></b>.</small>
										<a href="<?php the_permalink(get_page_by_path('tae')); ?>"><?php pll_e('APR'); ?>&nbsp;</a><span><span class="display-apr"></span>%</span>
									</div>
								</div>
								<div class="footer">
									<div class="form-item">
										<label class="label" for="email"><?php the_field('email_label'); ?></label>
										<input type="email" name="email" id="email">
										<small><?php the_field('email_validation'); ?></small>
									</div>
									<div class="btn-wrapper">
										<button type="button" class="btn btn-primary"><?php the_field('button_text'); ?></button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php while(have_rows('sections')) : the_row(); ?>
		<?php if (get_row_layout() == 'steps_section'): ?>
			<?php if (have_rows('steps')) : ?>
			<section class="steps-section">
				<div class="container">
					<div class="row">
						<?php while(have_rows('steps')) : the_row(); ?>
						<div class="col-md-6 col-lg-3">
							<div class="step">
								<?php echo wp_get_attachment_image(get_sub_field('image'), 'step'); ?>
								<?php if(get_sub_field('heading')) : ?>
								<h2><?php echo get_sub_field('heading'); ?></h2>
								<?php endif; ?>
								<?php if(get_sub_field('content')) : ?>
								<p><?php echo get_sub_field('content'); ?></p>
								<?php endif; ?>
							</div>
						</div>
						<?php endwhile; ?>
					</div>
				</div>
			</section>
			<?php endif; ?>
		<?php elseif(get_row_layout() == 'services_section') : ?>
			<section class="services-section">
				<div class="container">
					<?php if(get_sub_field('heading')) : ?>
					<h2><?php echo get_sub_field('heading'); ?></h2>
					<?php endif; ?>
					<div class="row">
						<?php if(have_rows('list')) : ?>
						<div class="col-md-5 col-lg-4">
							<ul class="list-tick">
							<?php while(have_rows('list')) : the_row(); ?>
							<li><?php echo get_sub_field('text'); ?></li>
							<?php endwhile; ?>
						</div>
						<?php endif; ?>
						<?php if(have_rows('testimonials')) : ?>
						<div class="col-md-7 col-lg-8">
							<?php while(have_rows('testimonials')) : the_row(); ?>
							<div class="testimonial">
								<div class="gfx-wrapper">
									<?php echo wp_get_attachment_image(get_sub_field('image'), 'testimonial'); ?>
								</div>
								<div class="content-wrapper">
									<q><?php echo get_sub_field('quote'); ?></q>
									<span class="author"><?php echo get_sub_field('author'); ?></span>
								</div>
							</div>
							<?php endwhile; ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</section>
		<?php elseif(get_row_layout() == 'legal_section') : ?>
			<section class="legal-section">
				<div class="container">
					<?php if(get_sub_field('heading')) : ?>
					<h4><?php echo get_sub_field('heading'); ?></h4>
					<?php endif; ?>
					<?php if(get_sub_field('content')) : ?>
						<?php echo get_sub_field('content'); ?>
					<?php endif; ?>
				</div>
			</section>
		<?php elseif(get_row_layout() == 'cta_section') : ?>
			<section class="cta-section">
				<div class="container">
					<?php if(get_sub_field('heading')) : ?>
					<h2><?php echo get_sub_field('heading'); ?></h2>
					<?php endif; ?>

					<?php if(get_sub_field('cta_url') && get_sub_field('cta_text')) : ?>
					<a href="<?php echo get_sub_field('cta_url'); ?>" class="btn btn-primary"><?php echo get_sub_field('cta_text'); ?></a>
					<?php endif; ?>
				</div>
			</section>
		<?php endif; ?>
	<?php endwhile; ?>
<?php get_footer();