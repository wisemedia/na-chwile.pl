<?php
/**
* The Header for theme.
*
* Displays all of the <head> section and page header
*
* @package WordPress
* @subpackage CreditosParaEmprendedores
* @since CreditosParaEmprendedores 1.0
*/
?>
<!DOCTYPE html>
<!--[if IE 8]><html <?php language_attributes(); ?> class="no-js ie ie8 lte8 lt9"><![endif]-->
<!--[if IE 9]><html <?php language_attributes(); ?> class="no-js ie ie9 lte9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<?php wp_head(); ?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body class="<?php echo custom_body_class(); ?>" >
	<div id="page">
		<header class="main">
			<div class="container">
				<a href="<?php echo get_home_url(); ?>" class="logo"><?php echo wp_get_attachment_image(get_field('logo', 'options'), 'logo'); ?></a>
				<a href="#" class="btn-menu"><img src="<?php echo get_template_directory_uri(); ?>/images/menu.png" alt="Menu"/></a>
				<nav>
					<?php wp_nav_menu(array("theme_location" => 'primary', 'container' => false)); ?>
				</nav>
				<a href="#" class="btn-search"></a>
				<div class="search-wrapper">
					<?php get_template_part('searchform'); ?>
				</div>
			</div>
		</header>
