<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage CreditosParaEmprendedores
 * @since CreditosParaEmprendedores 1.0
 */
get_header();
?>
	<?php get_template_part('banner', 'index'); ?>
	<section class="main">
		<div class="container">
			
<?php $sHeadText = null; ?>
<?php if(is_category()) { 
	$sHeadText = __("Category:", "CreditosParaEmprendedores").' '.single_cat_title( '', false ); 
} ?>

			<header class="page-header">
				<h1><span><?php echo single_cat_title( '', false ); ?></span></h1>
				<?php echo category_description(); ?>
			</header>

			<main class="content">
				<?php if (have_posts()) : ?>

					<?php get_template_part('loop', 'index'); ?>

				<?php else: ?>
					
					<h2><?php _e('Sorry, nothing found.','CreditosParaEmprendedores'); ?></h2>

				<?php endif;  ?>

				<?php get_template_part('sidebar'); ?>

			</main>

			<?php
			$args = array(
	            'mid_size'           => 3,
	            'prev_text'          => __( '<span>&lt;</span>' ),
	            'next_text'          => __( '<span>&gt;</span>' ),
	            'screen_reader_text' => __( ' ' )
	        )
			?>
			<?php the_posts_pagination($args); ?>
		</div>
	</section>
<?php get_footer(); ?>