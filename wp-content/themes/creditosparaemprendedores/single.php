<?php
/**
 * The single post page template.
 *
 *
 * @package WordPress
 * @subpackage CreditosParaEmprendedores
 * @since CreditosParaEmprendedores 1.0
 */

get_header(); the_post(); ?>
	<main class="content">
		<div class="container">
			<div class="post-wrapper">
				<article class="post">
					<div class="gfx">
						<?php the_post_thumbnail('post-thumb'); ?>
						<small class="post-info"><?php the_category(); ?><span class="date"><?php echo get_the_date(); ?></span></small>
					</div>
					<div class="content">
						<div class="post-entry">
							<h1 class="post-title"><?php the_title(); ?></h1>
							<?php the_content(); ?>
						</div>
						<?php if(get_the_tag_list()) : ?>
						<div class="tags-wrapper">
							<?php echo get_the_tag_list('<span>Tags: </span>', ', '); ?> 
						</div>
						<?php endif; ?>
					</div>
				</article>
				<?php 
					$post_prev = get_previous_post(); 
					$post_next = get_next_post();
					if($post_prev || $post_next) : 
				?>
				<div class="post-pagination-wrapper">
					<?php if($post_prev) : ?>
						<a href="<?php echo get_post_permalink($post_prev->ID); ?>" style="background-image: url(<?php echo get_the_post_thumbnail_url($post_prev); ?>);">
							<div class="content prev">
								<span class="link">Poprzedni wpis</span>
								<h2 class="title"><?php echo $post_prev->post_title; ?></h2>
							</div>
						</a>
					<?php endif; ?>
					<?php if($post_next) : ?>
						<a href="<?php echo get_post_permalink($post_next->ID); ?>" style="background-image: url(<?php echo get_the_post_thumbnail_url($post_next); ?>);">
							<div class="content next">
								<span class="link">Następny wpis</span>
								<h2 class="title"><?php echo $post_next->post_title; ?></h2>
							</div>
						</a>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			</div>
			
			<?php get_template_part('sidebar') ?>
		</div>
	</main>

<?php get_footer(); ?>