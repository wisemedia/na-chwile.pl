var $ = jQuery.noConflict();
var project = {};

jQuery(document).ready(function ($) {
  'use strict';
  $.extend(window.project, {
    initParam: {
      period: 65,
      currency: '€',
      days: 'days',
      price_percentage: 0.7077,
      percentage_step: 0.0,
      free_loan_from_principal_amount: 50,
      free_loan_to_principal_amount: 105,
      free_loan: false
    },
    parameters: {},

    init: function () {
      $('html').removeClass('no-js');

      $('.btn-menu').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('active');
      });

      $('.btn-search').on('click', function(e){
        e.preventDefault();
        $('.search-wrapper').toggleClass('active');
      })

      if($('#slider-form').length){
        project.func('getParameters');
      }
      project.func('slider');
      project.func('menu');

      $('.display-currency').text(project.initParam.currency);
      $('.display-days').text(project.initParam.days);
      $('.display-min-amount').text(project.parameters.min_sum);
      $('.display-max-amount').text(project.parameters.max_sum);

      if( $('[data-step]').length ){
        project.form.steps();
      }

      if( $('form#slider-form').length ){
        project.form.slider();
      }

      if( $('form#loan-form').length ){
        project.form.init();
      }

      $('.datepicker.birthdate').datepicker({
        yearRange: (project.helpers.getCurrentYear() - 75) + ':' + (project.helpers.getCurrentYear() - 18),
        minDate: '-75Y',
        maxDate: '-18Y',
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd'
      });

      $('.datepicker.employment').datepicker({
        maxDate: '-1',
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd'
      });

      $('select').selectmenu();
    },
    getParameters: function() {
      $.ajax({
        url: creditosparaemprendedores.ajax_url,
        method: 'POST',
        data: '&action=get_parameters',
        success: function(response) {
          var response = $.parseJSON($.parseJSON(response).body);
          $.extend(project.parameters, project.initParam, response);
          if (response.error !== "Unauthorized") {
            $('#slider-form').removeClass('loading');
          }
        },
        error: function(response) {
        },
        complete: function(response) {
          $('.display-min-amount').text(project.parameters.min_sum);
          $('.display-max-amount').text(project.parameters.max_sum);
          $('.display-min-period').text(project.parameters.min_period);
          $('.display-max-period').text(project.parameters.max_period);
          $('#loan_amount').data('min',project.parameters.min_sum).data('max',project.parameters.max_sum).data('default',project.parameters.default_sum).data('step',project.parameters.sum_step);
          $('#loan_period').data('min',project.parameters.min_period).data('max',project.parameters.max_period).data('default',project.parameters.default_period).data('step',project.parameters.period_step);
          project.func('slider');
        }
      });
    },
    slider: function(){

      $('.slider').each(function(){
        var _this = $(this);
        var form = _this.closest('form');
        var wrapper = _this.closest('.slider-wrapper');
        var min = _this.data('min');
        var max = _this.data('max');
        var step = _this.data('step');
        var value = _this.data('default');

        project.sliders.create(_this, {
          min: min,
          max: max,
          step: step,
          value: value,
          range: 'min',
          slide: project.sliders.update,
          change: project.sliders.update
        });

        
      });
    },

    menu : function(){
      $('.btn-menu').on('click', function(e){
        e.preventDefault();

        $(this).toggleClass('active');

      });
    },

    //builder
    func: function (fnName, selector, argArray) {
      if (typeof selector === 'undefined') {
        selector = 'body';
      }

      if (typeof argArray === 'undefined') {
        argArray = [];
      }

      argArray.unshift(selector);
      if (typeof project[fnName] === 'function' && jQuery(selector).length > 0) {
        project[fnName].apply(this, argArray);
      }

    },

    sliders: {
      utils: {
        displayValue: function (slider, value) {
          $('.slider-value', slider).text(value);
          project.loan.update(slider, parseInt(value));
        }
      },

      create: function (selector, params) {
        var slider = $(selector).slider(params);
        var wrapper = $(selector).closest('.slider-wrapper');
        $('.slider-less', wrapper).on('click', function(e){
          e.preventDefault();
          slider.slider('value', slider.slider('value') - params.step);
        });
        $('.slider-more', wrapper).on('click', function(e){
          e.preventDefault();
          slider.slider('value', slider.slider('value') + params.step);
        });
        this.utils.displayValue(slider, params.value);
      },

      update: function (event, ui) {
        var slider = $(this);
        project.sliders.utils.displayValue(slider, ui.value);
      }
    },

    loan: {

      amount: $('.loan-amount'),
      cost: $('.loan-cost'),
      total: $('.loan-sum'),
      period: $('.loan-period'),
      repayment: $('.loan-repayment'),

      update: function(element, value) {
        var slider_id = element.attr('id');

        if (slider_id.indexOf('loan_amount') != -1) {
          this.amount.text(value);
          $('input[name="loan_amount"]').val(value);
        }
        if (slider_id.indexOf('loan_period') != -1) {
          this.period.text(value);
          $('input[name="loan_period"]').val(value);
          $('.display-period').text(value);
        }
        var loan_repayment = project.helpers.getDateAfterDays($('input[name="loan_period"]').val());
        loan_repayment = loan_repayment.day + '.' + loan_repayment.month + '.' + loan_repayment.year;
        this.repayment.text(loan_repayment);
        $('input[name="loan_repayment"]').val(loan_repayment);
        $('.display-min-amount').text(project.parameters.min_sum);
        $('.display-max-amount').text(project.parameters.max_sum);
        $('.slider-value', element).text(value);

        var loan_amount = parseInt($('input[name="loan_amount"]').val());
        var loan_period = parseInt($('input[name="loan_period"]').val());
        var loan_cost = this.calculate.cost(loan_amount, loan_period);
        var loan_apr = this.calculate.apr(loan_amount, loan_period, loan_cost);
       
        if(typeof loan_cost != 'undefined'){
          var loan_sum = this.calculate.total(loan_amount, loan_cost);
          loan_cost = parseFloat(loan_cost).toFixed(2);
          loan_sum = parseFloat(loan_sum).toFixed(2);

          this.cost.text(loan_cost);
          this.total.text(loan_sum);
          $('input[name="loan_cost"]').val(loan_cost);
          $('input[name="loan_sum"]').val(loan_sum);
          $('input[name="loan_apr"]').val(loan_apr);
          $('.display-apr').text(loan_apr);
        }
      },

      calculate: {

        cost: function(amount, period) {
          if(amount && period) {
            if (project.parameters.free_loan && project.helpers.free_loan_between_range(amount)) {
              return 0.0;
            } else {
              var price_index = (project.parameters.max_sum - amount) / project.parameters.sum_step,
                period_index = (project.parameters.max_period - period) / project.parameters.period_step,
                percentage = (price_index + period_index) * project.initParam.percentage_step + project.initParam.price_percentage,
                price = percentage * 0.01 * amount * period;
              return price;
            }
          }
        },
        apr: function(amount, period, cost) {
          if(amount && period) {
            if (project.parameters.free_loan && project.helpers.free_loan_between_range(amount)) {
              return 0.0;
            } else {
              return 737.33;
            }
          }
        },

        total: function(amount, cost) {
          return amount + cost;
        }
      }
    },

    form: {
      init: function() {
        var _this = this;
        var request = {};
        var form = $('form#loan-form');

        _this.validate(form);

        $('input', form).on('change', function(){
          var allSteps = $('.step', form).not('.thank-you-step').length;
          var step = $(this).closest('.step');
          var currentStep = step.attr('id');
          currentStep = parseInt(currentStep.substring(currentStep.length - 1));
          var fields = $('.form-item', step);
          var allFields = fields.length;
          var okFields = 0;

          $(fields).each(function(){
            if($('input', $(this)).is('[type="text"],[type="password"],[type="email"]') && !$('input', $(this)).hasClass('error') && $('input', $(this)).val()!='') {
              okFields++;
            }else if($('input', $(this)).is('[type="radio"],[type="checkbox"]') && !$('input', $(this)).hasClass('error') && $('input', $(this)).is(':checked')){
              okFields++;
            }else if($('select, textarea', $(this)).length && !$('select, textarea', $(this)).hasClass('error') && $('select, textarea', $(this)).val() != ''){
              okFields++;
            }
          });

          var percentage = okFields/allFields;
          percentage = 100/allSteps*currentStep + (percentage/allSteps)*100;
          $('.form-steps-wrapper .form-progress').text(percentage.toFixed(0)+'%');
          $('.steps-indicator span').css({ width: percentage+'%' });
        });

        $('form#loan-form').on('submit', function(e){
          e.preventDefault();
          var form = $(this);

          _this.validate(form);
          $('input', form).trigger('validate');
          if(!$('.error', form).length){
            $.ajax({
              url: creditosparaemprendedores.ajax_url,
              method: 'POST',
              data: form.serialize() + '&action=send_application_loan',
              beforeSend: function() {
                $(form).addClass('loading');
              },
              success: function(response) {
                console.log(response);
                if($.parseJSON(response).body){
                  response = $.parseJSON($.parseJSON(response).body);
                  console.log(response);
                  if(response.result == 'ok'){
                    $('.thank-you.accepted').show().css({ display: 'flex'});
                    // dataLayer.push({'event': 'leadSubmitted', 'eventCategory': 'Lead submitted', 'eventAction': 'Accepted', 'eventLabel': response.loan_identifier});
                  }else if(response.status == 202){
                    $('.thank-you.rejected').show().css({ display: 'flex'});
                    // dataLayer.push({'event': 'leadSubmitted', 'eventCategory': 'Lead submitted', 'eventAction': 'Rejected', 'eventLabel': '666'});
                  }else {
                    $('.thank-you.api-error').show().css({ display: 'flex'});
                    // dataLayer.push({'event': 'leadSubmitted', 'eventCategory': 'Lead submitted', 'eventAction': 'Error', 'eventLabel': '-1'});
                  }
                }else{
                  $('.thank-you.api-error').show().css({ display: 'flex'});
                  // dataLayer.push({'event': 'leadSubmitted', 'eventCategory': 'Lead submitted', 'eventAction': 'Error', 'eventLabel': '-1'});
                }
              },
              error: function(response) {
                console.log(response);
                $('.thank-you.api-error').show().css({ display: 'flex'});
                // dataLayer.push({'event': 'leadSubmitted', 'eventCategory': 'Lead submitted', 'eventAction': 'Error', 'eventLabel': '-2'});

              },
              complete: function() {
                $(form).removeClass('loading');
                $('.form-wrapper, .form-steps-wrapper').hide();
                $('.thank-you-wrapper').show();
                $('.thank-you-wrapper').css({ minHeight: $(window).height() - $('.form-steps-wrapper').outerHeight() - $('#page > header').outerHeight() - $('#page > footer').outerHeight() });
              }
            });
          }

          return false;
        });
      },

      steps: function() {
        var _this = this;

        $('.step button[type="button"]').on('click', function() {

          var parent = $(this).closest('.step');
          var current = parent.attr('id').replace('step-','');
          var step = $(this).data('step');

          if(step > current) {
            _this.validate(parent);
            $('input', parent).trigger('validate');

            if(!$('.error', parent).length){
              _this.changeStep(step, parent);
            }
          }else{
            $('input', parent).removeClass('error');
            _this.changeStep(step, parent);
          }
        });
      },

      changeStep: function(step, parent) {
        parent.hide();
        $('#step-'+step).fadeIn();
        $('.step-description', '.form-steps-wrapper').hide();
        $('.step-description.step-'+step, '.form-steps-wrapper').fadeIn();
        $('.steps-wrapper > div').removeClass('active');
        $('.steps-wrapper > div').eq(step).addClass('active');

        $('html, body').animate({ scrollTop: 0 }, 500);
      },

      validate: function(form) {
        var _this = this;

        var notEmptyTest = function(elem){
          return $(elem).val().length && $(elem).val()!='';
        };
        var lengthTest = function(elem, min, max){
          return notEmptyTest($(elem)) && $(elem).val().length>=min && $(elem).val().length<=max;
        };
        var regexTest = function(elem, regex){
          return notEmptyTest($(elem)) && regex.test($(elem).val());
        };
        var ageTest = function(elem, min, max){
          if(notEmptyTest($(elem))){
            var date = $(elem).datepicker('getDate');
            var dateYear = date.getFullYear();
            var dateMonth = date.getMonth();
            var dateDay = date.getDate();
            var currentDate = new Date();
            var currentDateYear = currentDate.getFullYear();
            var currentDateMonth = currentDate.getMonth();
            var currentDateDay = currentDate.getDate();

            var isOverMin = false;
            if( currentDateYear - dateYear > min ) {
              isOverMin = true;
            }else if( currentDateYear - dateYear == min && currentDateMonth - dateMonth > 0 ){
              isOverMin = true;
            }else if( currentDateYear - dateYear == min && currentDateMonth - dateMonth == 0 && currentDateDay - dateDay >= 0){
              isOverMin = true;
            }

            var isBelowMax = false;
            if( currentDateYear - dateYear < max ) {
              isBelowMax = true;
            }else if( currentDateYear - dateYear == max && currentDateMonth - dateMonth < 0 ){
              isBelowMax = true;
            }else if( currentDateYear - dateYear == max && currentDateMonth - dateMonth == 0 && currentDateDay - dateDay <= 0){
              isBelowMax = true;
            }

            return isOverMin && isBelowMax;
          }
          return false;
        };

        $(form).each(function(i, form){
          $('input:not([type="hidden"]), input, textarea, select', form).not('.optional').on('validate', function(){
            _this.markError(this, notEmptyTest($(this)) );
          });

          $('input[type="radio"], input[type="checkbox"]', form).not('.optional').off('validate').on('validate', function(){
            var name = $(this).attr('name');
            _this.markError(this.closest('label'), $('input[name="'+name+'"]').is(':checked') );
          });

          $('input[name*="name"], input[name*="street"], input[name*="city"]', form).not('.optional').off('validate').on('validate', function(){
            _this.markError(this, lengthTest($(this), 2, 255) );
          });

          $('input[name="birth_date"]', form).not('.optional').off('validate').on('validate', function(){
            _this.markError(this, ageTest($(this), 18, 80) );
          });

          $('input[name*="phone"]', form).not('.optional').off('validate').on('validate', function(){
            var regex = /^(6[0-9]|7[1-9])[0-9]{7}$/;
            _this.markError(this, regexTest($(this), regex) );
          });

          $('input[type="email"]', form).not('.optional').off('validate').on('validate', function(){
            var regex = /^([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})$/;
            if($(this).hasClass('conditional')){
              var value = $('input[name="'+$(this).data('condition')+'"]').val();
              _this.markError(this, regexTest($(this), regex) && $(this).val()==value);
            }else{
              _this.markError(this, regexTest($(this), regex));
            }
          });

          $('input[type="password"]', form).not('.optional').off('validate').on('validate', function(){
            if($(this).hasClass('conditional')){
              var value = $('input[name="'+$(this).data('condition')+'"]').val();
              _this.markError(this, lengthTest($(this), 6, 20) && $(this).val()==value);
            }else{
              _this.markError(this, lengthTest($(this), 6, 20));
            }
          });

          $('input[name="house_number"]', form).not('.optional').off('validate').on('validate', function(){
            var regex = /^\d+$/;
            _this.markError(this, regexTest($(this), regex));
          });

          $('input[name*="zip_code"]', form).not('.optional').off('validate').on('validate', function(){
            var regex = /^(0[1-9]|[1-4]\d|5[0-2])\d{3}$/;
            _this.markError(this, regexTest($(this), regex));
          });

          $('input[name*="income"]', form).not('.optional').off('validate').on('validate', function(){
            var regex = /^\d+$/;
            _this.markError(this, regexTest($(this), regex) && $(this).val()>=100 && $(this).val()<=10000);
          });

          $('input[name*="dependant_count"]', form).not('.optional').off('validate').on('validate', function(){
            var regex = /^\d+$/;
            _this.markError(this, regexTest($(this), regex) && $(this).val()>=0);
          });

          $('input[name*="employment_since"]', form).not('.optional').off('validate').on('validate', function(){
            _this.markError(this, ageTest($(this), 0, 100));
          });

          $('input[name*="personal_code"]', form).not('.optional').off('validate').on('validate', function(){
            var regex = /^([xyXY]\-?|\d)\d{7}\-?[a-zA-Z]$/;
            _this.markError(this, regexTest($(this), regex));
          });

          $('input[name="bank_account_number_without_prefix"]', form).not('.optional').off('validate').on('validate', function(){
            var regex = /^\d{22}$/;
            _this.markError(this, regexTest($(this), regex));
          });

          $('input, textarea, select', form).not('.optional').on('change', function(){
            $(this).trigger('validate');
          });
        });
      },

      markError: function(elem, test, message){
        if(test) {
          $(elem).removeClass('error');
        }
        else {
          $(elem).addClass('error');
        }
      },

      slider: function() {
        $('button', 'form#slider-form').on('click', function(e){
          e.preventDefault();
          var form = $(this).closest('form#slider-form');

          project.form.validate(form);
          $('input', form).trigger('validate');

          if(!$('.error', form).length){
            form.trigger('submit');
          }
        });
      }
    },

    helpers: {
      getDateAfterDays: function (days) {
        var date = new Date(new Date().getTime() + (days * 24 * 60 * 60 * 1000)),

          formatted = {
            day: this.addZeroToDate(date.getDate()),
            month: this.addZeroToDate(date.getMonth() + 1),
            year: date.getFullYear()
          };

        return formatted;
      },

      getCurrentYear: function () {
        return new Date().getFullYear();
      },

      addZeroToDate: function (n) {
        return n < 10 ? '0' + n : n;
      },

      displayNextStep: function (button, current, next, action) {
        button.click(function () {
          current.hide();
          next.fadeIn();
          action();
        });
      },

      toTitleCase: function (strings) {
        for(var i=0; i < strings.length; i++){
          strings[i] = strings[i].replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }
        return strings;
      },
      free_loan_between_range: function (amount) {
        if (amount >= project.parameters.free_loan_from_principal_amount && amount < project.parameters.free_loan_to_principal_amount) {
          return true;
        } else {
          return false;
        }
      }
    },
  });

  project.init();
});