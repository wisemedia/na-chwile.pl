<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage CreditosParaEmprendedores
 * @since CreditosParaEmprendedores 1.0
 */
get_header();
?>
	<?php get_template_part('banner', 'index'); ?>
	<section class="main">
		<div class="container">
			
<?php $sHeadText = null; ?>
<?php if(is_archive()) { 
	if(is_day()) :
		$sHeadText = __('Daily Archives:', 'CreditosParaEmprendedores') . get_the_date();
	elseif(is_month()) :
		$sHeadText = __('Monthly Archives:', 'CreditosParaEmprendedores') . get_the_date(_x('F Y', 'monthly archives date format', 'CreditosParaEmprendedores'));
	elseif(is_year()) :
		$sHeadText = __('Yearly Archives:', 'CreditosParaEmprendedores') . get_the_date(_x('Y', 'yearly archives date format', 'CreditosParaEmprendedores'));
	else :
		$sHeadText = __('Blog Archives', 'CreditosParaEmprendedores');
	endif;
} ?>
<?php if(is_category()) { 
	$sHeadText = __("Category:", "CreditosParaEmprendedores").' '.single_cat_title( '', false ); 
} ?>
<?php if(is_author()) { 
	$authordata = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
	$sHeadText = __("Author:", "CreditosParaEmprendedores").' '.$authordata->display_name; 
} ?>
<?php if(is_tag()) { 
	$sHeadText = __("Tag:", "CreditosParaEmprendedores").' '.single_tag_title( '', false ); 
} ?>
<?php if(is_search()) { 
	$sHeadText = __("Search:", "CreditosParaEmprendedores").' '.get_search_query(); 
} ?>

			<main class="content">
				<?php if (have_posts()) : ?>

					<header class="page-header">
						<h1><span><?php the_field('blog_title', 'options'); ?></span></h1>
						<?php the_field('blog_description', 'options'); ?>
					</header>

					<?php get_template_part('loop', 'index'); ?>

				<?php else: ?>
					
					<h2><?php _e('Sorry, nothing found.','CreditosParaEmprendedores'); ?></h2>

				<?php endif;  ?>

			</main>

			<?php if(is_archive() || is_category()) { 
				get_template_part('sidebar');
			} ?>

			<?php
			$args = array(
	            'mid_size'           => 3,
	            'prev_text'          => __( '<span>&lt;</span>' ),
	            'next_text'          => __( '<span>&gt;</span>' ),
	            'screen_reader_text' => __( ' ' )
	        )
			?>
			<?php the_posts_pagination($args); ?>
		</div>
	</section>
<?php get_footer(); ?>