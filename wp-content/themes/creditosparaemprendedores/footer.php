<?php
/**
 * The footer for theme.
 *
 *
 * @package WordPress
 * @subpackage CreditosParaEmprendedores
 * @since CreditosParaEmprendedores 1.0
 */
?>
		<footer class="main">
			<div class="container">
				<div class="row">
					<?php if(is_active_sidebar('footer_widget_area')) : ?>
						<?php dynamic_sidebar('footer_widget_area'); ?>
					<?php endif; ?>
				</div>
		</footer>
	</div> <!-- /#page --> 
<?php wp_footer(); ?>
</body>
</html>