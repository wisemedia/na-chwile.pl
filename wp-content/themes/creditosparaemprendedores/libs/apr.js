//APR
var APR = {
  test_params: {
    "sums": [
      200,
      100,
      100,
      100,
      50,
      100,
      100,
      100,
      150,
      100,
      -1247
    ],
    "periods": [
      0,
      30,
      58,
      91,
      119,
      150,
      182,
      211,
      242,
      273,
      289
    ],
    "apr": 32.3,
    "example_name": "http://www.experiglot.com"
  }
};

APR.value_or_default = function(value,default_value) {
  return typeof value !== 'undefined' ? value : default_value;
};

//XIRR
APR.XIRR = function(opts) {
  if (this === APR) throw "usage: new APR.XIRR(sums,periods,opts)";
  this.opts = opts;
  this.sums = null;
  this.periods = null;
  this.d_0 = null;
  this.e_i = null;
  this.e_i_times_sums = null;

  var MAX_ITER = 50;
  var MAX_EPS = 1e-5;
  var INITIAL_GUESS = 0.1;
  
  this.sums = this.opts.sums;
  this.periods = this.opts.periods;
  this.guess = APR.value_or_default(this.opts.guess,INITIAL_GUESS);

  this.set_sums = function(sums) {
    this.sums = sums;
  };

  this.set_periods = function(periods) {
    this.periods = periods;
  };

  this.set_guess = function(guess) {
    this.guess = guess;
  };

  this.pre_calculate = function() {
    this.d_0 = this.periods[0];
    this.e_i = this.periods.map(function(date){
      return (date-this.d_0) / 365.0;
    },this);
    this.e_i_times_sums = this.e_i.map(function(e_i_el, index){
      return e_i_el * this.sums[index];
    },this);
    this.result_rate = this.guess;
  };

  this.xirr = function() {
    var r = this.result_rate + 1.0;
    var result = this.sums[0];
    for (var i = 1; i < this.sums.length; i++) {
      result += this.sums[i] / (Math.pow(r, this.e_i[i]));
    }
    return result;
  };

  this.xirr_deriv = function() {
    var r = this.result_rate + 1.0;
    var result = 0.0;
    for (var i = 1; i < this.sums.length; i++) {
      result -= this.e_i_times_sums[i] / (Math.pow(r, this.e_i[i] + 1.0));
    }
    return result;
  }; 

  this.continue_iterate = function(){
    return this.continue_loop;
  };

  this.iterate = function() {
    var result_value = this.xirr();
    var result_value_deriv = this.xirr_deriv();
    var new_rate = this.result_rate - result_value / result_value_deriv;
    var rate_eps = Math.abs(new_rate - this.result_rate);
    this.continue_loop = (rate_eps > MAX_EPS) && (Math.abs(result_value) > MAX_EPS);
    this.result_rate = new_rate;
  };

  this.validate_params = function() {
    if (this.sums.length < 2) throw "At least two elements required";
    if (this.sums.length != this.periods.length) throw "sums.count != periods.count";
  };

  this.calculate_xirr = function(name) {
    this.validate_params();
    this.pre_calculate();
      
    for (var i = 0; i < MAX_ITER; i++) {
      this.iterate();
      if (!this.continue_iterate()) return this.result_rate;
    }
    throw "max iterations reached";
  };

  return this;
};

APR.CalculatorShortTerm = function(opts) {
  if (this === APR) throw "usage: new APR.Calculator()";
  this.opts = APR.value_or_default(opts,{});
  this.sum = this.opts.sum;
  this.period = this.opts.period;
  this.fee = APR.value_or_default(this.opts.fee,0);
  this.needs_calculate = true;

  this.set_sum = function(sum) {
    this.sum = sum;
    this.needs_calculate = true;
  };

  this.set_period = function(period) {
    this.period = period;
    this.needs_calculate = true;
  };

  this.set_fee = function(fee) {
    this.fee = fee;
    this.needs_calculate = true;
  };

  this.calculate = function() {
    if (this.needs_calculate) {
      this.raw_apr = Math.pow(((0.0 + this.sum + this.fee) / (this.sum)),(365.0/this.period)) - 1;
      this.needs_calculate = false;
    }
    return this;
  };

  this.to_percent = function(){
    return Math.round(this.raw_apr * 1000.0) / 10;
  };

  this.apr = function() {
    this.calculate();
    return this.to_percent();
  };

  return this;
};


//Calculator
APR.CalculatorLongTerm = function(opts) {
  if (this === APR) throw "usage: new APR.Calculator()";
  this.opts = APR.value_or_default(opts,{});
  this.xirr = new APR.XIRR(this.opts);
  this.needs_calculate = true;

  this.set_sums = function(sums) {
    this.xirr.set_sums(sums);
    this.needs_calculate = true;
    return this;
  };

  this.set_periods = function(periods) {
    this.xirr.set_periods(periods);
    this.needs_calculate = true;
    return this;
  };

  this.set_guess = function(guess) {
    this.xirr.set_guess(guess);
    this.needs_calculate = true;
    return this;
  };

  this.calculate = function() {
    if (this.needs_calculate) {
      this.raw_apr = this.xirr.calculate_xirr();
      this.needs_calculate = false;
    }
    return this;
  };

  this.to_percent = function(){
    return Math.round(this.raw_apr * 1000.0) / 10;
  };

  this.apr = function() {
    this.calculate();
    return this.to_percent();
  };

  return this;
};
